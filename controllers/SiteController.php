<?php

namespace app\controllers;

use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTest()
    {
        $sql = 'SELECT u.name, (SELECT c.text FROM comments as c WHERE c.user_id=u.id ORDER BY c.id DESC LIMIT 1) as text FROM users as u';
        $info = Yii::$app->db->createCommand($sql)->queryAll();
        var_dump($info);
    }


}
