<?php

use yii\db\Migration;


/**
 * Class m180131_082355_add_test_data
 */
class m180131_082355_add_test_data_users extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $usersData = [
          ['kolya', 300.00],
          ['elena', 1300.00],
          ['vadim', 900.00],
          ['sergey', 657.35],
          ['boris',  549.70],
        ];

        $this->batchInsert('users', ['name', 'balance'],$usersData);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable('users');
    }

}
