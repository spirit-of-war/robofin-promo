<?php

use \app\models\Users;
use yii\db\Migration;

/**
 * Class m180131_084750_add_test_data_comments
 */
class m180131_084750_add_test_data_comments extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $users = Users::find()->all();
        $userIdArr = [];
        foreach($users as $user) {
            $userIdArr[] = $user->id;
        }

        $commentsData = [
            [$userIdArr[0], 'Тестовый текст'],
            [$userIdArr[0], 'Тестовый текст но не тот'],
            [$userIdArr[1], 'Тестовый текст и этот тоже '],
            [$userIdArr[2], 'Тестовый текст и вот этот '],
            [$userIdArr[2], 'Тестовый текст куда же без него'],
            [$userIdArr[3], 'Тестовый текст ведь совсем красный'],
            [$userIdArr[4], 'Тестовый текст не синий'],
            [$userIdArr[4], 'Тестовый текст ну может чутка синеват'],
            [$userIdArr[4], 'Тестовый текст а хотя нет'],
        ];

        $this->batchInsert('comments', ['user_id', 'text'],$commentsData);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable('comments');
    }

}
