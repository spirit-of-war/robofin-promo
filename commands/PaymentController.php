<?php

namespace app\commands;

use \yii\console\Controller;
use \app\models\Users;

class PaymentController extends Controller
{
    public function actionStart($userSender, $userRecipient, $amount)
    {
        if (!$this->checkStrOnFloat($amount)) {
            throw new \Exception('Format amount must be int or float');
        }
        $amount = floatval($amount);
        $sender = Users::find()->where(['name' => $userSender])->one();
        $recipient = Users::find()->where(['name' => $userRecipient])->one();

        if (!$sender) {
            throw new \Exception('Sender is not valid');
        }
        if (!$recipient) {
            throw new \Exception('Recipient is not valid');
        }
        if ($sender->balance < $amount) {
            throw new \Exception('Error operation. Not enough money');
        }


        $transaction = \Yii::$app->db->beginTransaction();

        try {
            $sender->balance = $sender->balance - $amount;
            $recipient->balance = $recipient->balance + $amount;
            $sender->save();
            $recipient->save();

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }

    private function checkStrOnFloat($subject)
    {
        $pattern = '/^-?(?:\d+|\d*\.\d+)$/';
        return preg_match($pattern, $subject);
    }
}